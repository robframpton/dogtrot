import React, { Component } from 'react';

import FormContainer from './components/FormContainer';
import StepFour from './components/StepFour';
import StepOne from './components/StepOne';
import StepThree from './components/StepThree';
import StepTwo from './components/StepTwo';

import './App.css';

class App extends Component {
	render() {
		return (
			<div className="dogtrot">
				<h1 className="dogtrot-title"><a href="/">DogTrot</a></h1>

				<FormContainer
					steps={[StepOne, StepTwo, StepThree, StepFour]}
				/>
			</div>
		);
	}
}

export default App;
