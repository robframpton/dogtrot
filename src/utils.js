export function getRefValues(component) {
	return Object.keys(component.refs).reduce((result, key) => {
		result[key] = component.refs.value;

		return result;
	}, {});
}