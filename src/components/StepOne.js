import React, { Component } from 'react';

import StepTransition from './StepTransition';

class StepOne extends Component {
	render() {
		return (
			<div className="step-one">
				<StepTransition>
					<div className="form-group">
						<label htmlFor="name">Your name</label>

						<input type="text" className="form-control" id="name" name="name" />
					</div>

					<div className="form-group">
						<label htmlFor="phone">Phone</label>

						<input type="text" className="form-control" id="phone" name="phone" />
					</div>
				</StepTransition>
			</div>
		);
	}
}

export default StepOne;
