import React, { Component } from 'react';

import StepTransition from './StepTransition';

class StepTwo extends Component {
	render() {
		return (
			<div className="step-two">
				<StepTransition>
					<div className="form-group">
						<label htmlFor="city">City</label>

						<input type="text" className="form-control" id="city" name="city" />
					</div>

					<div className="form-group">
						<label htmlFor="time">Time of day</label>

						<input type="time" className="form-control" id="time" name="time" />
					</div>
				</StepTransition>
			</div>
		);
	}
}

export default StepTwo;
