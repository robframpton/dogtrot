import React, { Component } from 'react';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';

class StepTransition extends Component {
	render() {
		return (
			<ReactCSSTransitionGroup
				transitionAppear={true}
				transitionAppearTimeout={500}
				transitionEnter={false}
				transitionLeave={false}
				transitionName="step-transition"
			>
				{this.props.children}
			</ReactCSSTransitionGroup>
		);
	}
}

export default StepTransition;
