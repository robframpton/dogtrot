import React, { Component } from 'react';

import StepTransition from './StepTransition';

class StepThree extends Component {
	render() {
		return (
			<div className="step-three">
				<StepTransition>
					<div className="form-group">
						<label htmlFor="dogName">Dog's name</label>

						<input type="text" className="form-control" id="dogName" name="dogName" />
					</div>

					<div className="form-group">
						<label htmlFor="breed">Breed</label>

						<input type="text" className="form-control" id="breed" name="breed" />
					</div>

					<div className="form-check">
						<input className="form-check-input" type="checkbox" id="goodWithDogs" name="goodWithDogs" />

						<label className="form-check-label" htmlFor="goodWithDogs">
							Good with other dogs
						</label>
					</div>
				</StepTransition>
			</div>
		);
	}
}

export default StepThree;
