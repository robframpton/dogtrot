import React, { Component } from 'react';
import Slider from 'rc-slider';

import StepTransition from './StepTransition';

class StepFour extends Component {
	render() {
		return (
			<div className="step-two">
				<StepTransition>
					<div className="form-group">
						<label>Approximate size</label>

						<div className="slider-legend">
							<span className="slider-legend-min">Small</span>
							<span className="slider-legend-max">Large</span>
						</div>

						<Slider
							dots={true}
							max={5}
							min={1}
						/>
					</div>

					<div className="form-group">
						<label>Energy level</label>

						<div className="slider-legend">
							<span className="slider-legend-min">Calm</span>
							<span className="slider-legend-max">High-strung</span>
						</div>

						<Slider
							dots={true}
							max={5}
							min={1}
						/>
					</div>

					<div className="custom-file">
						<input type="file" className="custom-file-input" id="dogPhoto" />
						<label className="custom-file-label" htmlFor="dogPhoto">Photo of dog</label>
					</div>
				</StepTransition>
			</div>
		);
	}
}

export default StepFour;
