import React, { Component } from 'react';
import PropTypes from 'prop-types';

import './StepProgress.css';

class StepProgress extends Component {
	render() {
		const { step, totalSteps } = this.props;

		return (
			<div className="step-progress">
				<ol className="list-unstyled">
					{[...Array(totalSteps)].map((item, index) => {
						let className = 'step-progress-item';

						if (index === step) {
							className += ' step-progress-item-current';
						}
						else if (index < step) {
							className += ' step-progress-item-done';
						}

						return (
							<li className={className} key={index}>
								<button
									className="indicator"
									onClick={this.onClick.bind(this, index)}
									type="button"
								></button>

								{(index !== totalSteps - 1) && <span className="separator"></span>}
							</li>
						);
					})}
				</ol>
			</div>
		);
	}

	onClick(index) {
		this.props.onClick(index);
	}
}

StepProgress.propTypes = {
	onClick: PropTypes.func,
	step: PropTypes.number,
	totalSteps: PropTypes.number
};

export default StepProgress;
