import PropTypes from 'prop-types';
import React, { Component } from 'react';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';

import StepProgress from './StepProgress';

import './FormContainer.css';

class FormContainer extends Component {
	constructor(props) {
		super(props);

		this.state = {
			data: {},
			finished: false,
			step: 0,
		};

		this.next = this.next.bind(this);
		this.onStepClick = this.onStepClick.bind(this);
		this.previous = this.previous.bind(this);
	}

	render() {
		return (
			<div className="form-container">
				{this.state.finished ? this.renderFinished() : this.renderForm()}
			</div>
		);
	}

	renderFinished() {
		return (
			<div className="form-finished">
				<ReactCSSTransitionGroup
					transitionAppear={true}
					transitionAppearTimeout={500}
					transitionEnter={false}
					transitionLeave={false}
					transitionName="finish-transition"
				>
					<h2>You're all done!</h2>

					<p>We'll be contacting you shortly to complete your registration</p>
				</ReactCSSTransitionGroup>
			</div>
		);
	}

	renderForm() {
		return (
			<form>
				<h2 className="form-header">Dog walking sign up</h2>

				<StepProgress
					step={this.state.step}
					onClick={this.onStepClick}
					totalSteps={this.props.steps.length}
				/>

				{this.renderStep()}

				<div className="btn-group step-controls" role="group" aria-label="Form controls">
					{this.hasPrevious() && <button className="btn" onClick={this.previous} type="button">
						Previous
					</button>}

					<button className="btn btn-next" onClick={this.next} type="button">
						{this.isLast() ? 'Finish' : 'Next'}
					</button>
				</div>
			</form>
		);
	}

	renderStep() {
		const StepComp = this.props.steps[this.state.step];

		return (
			<div className="step-container">
				<StepComp ref="stepComponent" />
			</div>
		);
	}

	hasNext() {
		return this.state.step < this.props.steps.length - 1;
	}

	hasPrevious() {
		return this.state.step > 0;
	}

	isLast() {
		return this.state.step === this.props.steps.length - 1;
	}

	next() {
		if (this.isLast()) {
			this.setState({
				finished: true
			});
		}
		else if (this.hasNext()) {
			this.setState({
				step: this.state.step + 1
			});
		}
	}

	onStepClick(step) {
		this.setState({
			step
		});
	}

	previous() {
		if (this.hasPrevious()) {
			this.setState({
				step: this.state.step - 1
			});
		}
	}
}

FormContainer.propTypes = {
	steps: PropTypes.array
};

export default FormContainer;
